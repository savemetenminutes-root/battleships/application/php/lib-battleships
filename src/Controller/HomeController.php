<?php

namespace Smtm\Battleships\Controller;

use Psr\Http\Message\RequestInterface;
use Smtm\Mvc\View\Model\ViewModel;

class HomeController
{
    protected $config;
    protected $request;
    protected $viewModel;
    protected $viewRenderer;

    public function __construct(
        array $config,
        RequestInterface $request,
        //ViewModel $viewModel
        $viewRenderer
    ) {
        $this->config    = $config;
        $this->request   = $request;
        //$this->viewModel = $viewModel;
        $this->viewRenderer = $viewRenderer;
    }

    public function home()
    {
        /*
        return
            $this
                ->viewModel
                ->setVariables(
                    [
                        'x' => $this->config['x'],
                        'y' => $this->config['y'],
                    ]
                )
                ->setTemplate('app::home-page')
            ;
        */
        return $this->viewRenderer->render('app::home-page');
    }
}
