<?php

namespace Smtm\Battleships\Controller\Factory;

use Psr\Container\ContainerInterface;
use Smtm\Battleships\Controller\HomeController;
use Smtm\Mvc\View\Model\ViewModel;
use Smtm\Psr\Container\Factory\FactoryInterface;
use Smtm\Psr\Http\Message\Request\CurrentRequestDecorator;

class HomeControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $configBattleships   = $container->get('config')['battleships'] ?? [];
        $currentRequest      = $container->get(CurrentRequestDecorator::class);
        $viewModel           = $container->get(ViewModel::class);
        $viewRendererFactory = new \Zend\Expressive\ZendView\ZendViewRendererFactory();
        $viewRenderer        = $viewRendererFactory($container);
        $controller          =
            new HomeController(
                $configBattleships,
                $currentRequest,
                //$viewModel
                $viewRenderer
            )
        ;
        return $controller;
    }
}
