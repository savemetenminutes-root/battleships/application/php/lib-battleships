<?php

declare(strict_types=1);

namespace Smtm\Battleships;

use Smtm\Battleships\Controller\Factory\ControllerAbstractFactory;
use Smtm\Battleships\Controller\HomeController;

/**
 * The configuration provider for the module
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'routes'       => $this->getRoutes(),
            'templates'    => $this->getTemplates(),
            'battleships'  => $this->getBattleships(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies(): array
    {
        return [
            'shared' => [
                'factories' => [
                    HomeController::class => ControllerAbstractFactory::class
                ],
            ]
        ];
    }

    /**
     * Returns the container routes
     */
    public function getRoutes(): array
    {
        return [
            'HOME' => [
                'path'     => '',
                'children' => [
                    'HOME_GET' => [
                        'method' => 'get',
                        'action' => [
                            'controller' => HomeController::class,
                            'method'     => 'home',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates(): array
    {
        return [
            'paths' => [
                'app'    => [__DIR__ . '/../templates/app'],
                'error'  => [__DIR__ . '/../templates/error'],
                'layout' => [__DIR__ . '/../templates/layout'],
            ],
        ];
    }

    public function getBattleships()
    {
        return [
            'x' => 10,
            'y' => 10,
        ];
    }
}
