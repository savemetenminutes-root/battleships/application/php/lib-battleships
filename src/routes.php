<?php

return [
    'home' => [
        Route::URL     => [
            '',
        ],
        Route::ACTIONS => [
            Route::METHOD_GET => [
                \Smtm\Battleships\HomeController::class => [
                    Route::CONTROLLER_METHOD => 'home',
                ],
            ],
        ],
    ],
];
